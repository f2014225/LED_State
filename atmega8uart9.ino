#include<avr/interrupt.h>
#include<avr/io.h>
#include <util/delay.h>
#include <stdint.h>
#include <compat/deprecated.h>
#define BAUD 9600                                 // define baud
#define BAUDRATE ((16000000)/(BAUD*16UL)-1)            // set baud rate value for UBRR,16000000 is the clock frequency
uint16_t lux_threshold_lower=0,lux_threshold_higher=0;    //to be set via setup policy
volatile int i=0,act=0;
volatile unsigned long count = 0;
volatile char command[10];
char lux1[4], lux2[4], temperature[4], voltage[4], current[4];
char bright = 'a';
char off='0'; // when LED has to be switched off
uint16_t current_temp,current_lux;
enum State{Fallback,Active,Waiting};
State state=Active;
char delimiter = ',';
ISR(USART_RXC_vect)
{
  char ReceivedByte;
  ReceivedByte = UDR;
  if(ReceivedByte == 10)
  {
    act = 1;
    i=0;
  } 
  else
  { 
    command[i]=ReceivedByte;
    i++;
    act =0;
  }
  state = Active;
  TCNT1 = 0;
  count=0;
}
ISR(TIMER1_COMPA_vect)
{
  count++;
  if((count!=0)&&(count%900==0))            // for 15 minutes
    state = Fallback;
}
uint16_t ReadADC(uint8_t ch)
//unsigned char ReadADC(uint8_t ch)
{
   //Select ADC Channel ch must be 0-7
   uint8_t channel;
   channel=ch&0b00000111;
   ADMUX|=channel;

   //Start Single conversion
   ADCSRA|=(1<<ADSC);

   //Wait for conversion to complete
   while(ADCSRA & (1<<ADSC));
   while(!(ADCSRA & (1<<ADIF)));

   //Clear ADIF by writing one to it
   //Note you may be wondering why we have write one to clear it
   //This is standard way of clearing bits in io as said in datasheets.
   //The code writes '1' but it results in setting bit to '0' !!!

   ADCSRA|=(1<<ADIF);

   return(ADC);
   //return(ADCH);
}
void InitADC()
{
  ADMUX=(1<<REFS0);                         // For Aref=AVcc;
  ADCSRA=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); //Prescalar div factor =128, pg
}
void set_brightness(char b)
{
	if(b=='0'){		
		sbi(PORTD,7);
		sbi(PORTB,2);
		sbi(PORTB,5);
		sbi(PORTD,5);
		sbi(PORTD,6);
		sbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}
    else if(b=='1'){
		cbi(PORTD,7);
		sbi(PORTB,2);
		sbi(PORTB,5);
		sbi(PORTD,5);
		sbi(PORTD,6);
		sbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}
	else if(b=='2'){
		cbi(PORTD,7);
		cbi(PORTB,2);
		sbi(PORTB,5);
		sbi(PORTD,5);
		sbi(PORTD,6);
		sbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}
	else if(b=='3'){
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		sbi(PORTD,5);
		sbi(PORTD,6);
		sbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}
    else if(b=='4'){
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		sbi(PORTD,6);
		sbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}	     
	else if(b=='5'){
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		cbi(PORTD,6);
		sbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}
	else if(b=='6'){
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		cbi(PORTD,6);
		cbi(PORTB,3);
		sbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}  	
    else if(b=='7'){
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		cbi(PORTD,6);
		cbi(PORTB,3);
		cbi(PORTB,4);
		sbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}	        
	else if(b=='8'){  
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		cbi(PORTD,6);
		cbi(PORTB,3);
		cbi(PORTB,4);
		cbi(PORTD,4);
		sbi(PORTD,2);
		sbi(PORTD,3);}
	else if(b=='9'){	 
		cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		cbi(PORTD,6);
		cbi(PORTB,3);
		cbi(PORTB,4);
		cbi(PORTD,4);
		cbi(PORTD,2);
		sbi(PORTD,3);}	
	else if(b=='a'){	 
                cbi(PORTD,7);
		cbi(PORTB,2);
		cbi(PORTB,5);
		cbi(PORTD,5);
		cbi(PORTD,6);
		cbi(PORTB,3);
		cbi(PORTB,4);
		cbi(PORTD,4);
		cbi(PORTD,2);
		cbi(PORTD,3);}
}
void get_temperature()

{
  int n=0;
  uint16_t adc_result1;
  ADCSRA &= ~((1<<ADEN));
  InitADC();
  _delay_ms(10);
  adc_result1=ReadADC(4);
  itoa(adc_result1,temperature,10);
  for(n=0;temperature[n]!='\0';n++)
    UDR=temperature[n];
  UDR = delimiter;
}
uint16_t get_temperature_Fallback()
{
  uint16_t adc_result1;
  ADCSRA &= ~((1<<ADEN));
  InitADC();
  _delay_ms(10);
  adc_result1=ReadADC(4);
  return(adc_result1);    //to compare with threshold limit;
}
void get_lux()
{
  int n;
  uint16_t adc_result1,adc_result2;
  ADCSRA &= ~((1<<ADEN));
  InitADC();
  _delay_ms(10);
  
  adc_result1=ReadADC(2);
  itoa(adc_result1,lux1,10);
  for(n=0;lux1[n]!='\0';n++)
    UDR = lux1[n];
  
  UDR = delimiter;
}
uint16_t get_lux_Fallback()
{
  uint16_t adc_result1,adc_result2;
  ADCSRA &= ~((1<<ADEN));
  InitADC();
  _delay_ms(10);
  
  adc_result1=ReadADC(2);
  
  return (adc_result1);      // assuming ambient lux sensor is connected to channel 2
}
void get_voltage()
{
  int n;
  uint16_t adc_result1;
  ADCSRA &= ~((1<<ADEN));
  InitADC();
  _delay_ms(10);

  adc_result1=ReadADC(1);
  itoa(adc_result1,voltage,10);
  for(n=0;voltage[n]!='\0';n++)
   UDR = voltage[n];
  UDR = delimiter;
}
void get_current()
{
  int n;
  uint16_t adc_result1;
  ADCSRA &= ~((1<<ADEN));
  InitADC();
  _delay_ms(10);
  adc_result1=ReadADC(3);
  itoa(adc_result1,current,10);
  for(n=0;current[n]!='\0';n++)
   UDR = current[n];
  UDR = delimiter;
}
void send_lux()
{
  uint8_t lux_lower, lux_higher;
  lux_lower = lux_threshold_lower/4;
  lux_higher = lux_threshold_higher/4;
  UDR = lux_lower;
  UDR = delimiter;
  UDR = lux_higher;
}
void Setup_Policy()
{
  //setup policy
}
void setup()
{
  Setup_Policy();
  UBRRH = (BAUDRATE>>8);                      // shift the register right by 8 bits
  UBRRL = BAUDRATE;                           // set baud rate
  UCSRB  = (1<<TXEN)|(1<<RXEN);                // enable receiver and transmitter
  UCSRC  = (1<<URSEL)|(1<<UCSZ0)|(1<<UCSZ1);   // 8bit data format
  UCSRB |= (1<<RXCIE);
  //set timer1 interrupt at 1Hz
  TCCR1A = 0;// set entire TCCR1A register to 0
  TCCR1B = 0;// same for TCCR1B
  TCNT1  = 0;//initialize counter value to 0
  // set compare match register for 1hz increments
  OCR1A =15624;// = [(16*10^6) / (1*1024) - 1] (must be <65536) for 1 second 
  // turn on CTC mode
  TCCR1B |= (1 << WGM12);
  // Set CS12 and CS10 bits for 1024 prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);  
  // enable timer compare interrupt
  TIMSK |= (1 << OCIE1A);
  sei();
  DDRB |= 0x3C; 
  DDRD |= 0xFC;
}
void loop()
{ 
  switch(state){
    case(Active):
    { 
      if(act == 1)
      {
        act = 0;
        switch(command[1])
        {
          case('s'):
          {
            get_temperature();
            get_lux();
            get_voltage();
            get_current();
            send_lux();
            UDR = 10;
            state=Waiting;
            break;
          }
          case('b'):
          {
            set_brightness(command[2]);
            state=Waiting;
            break;  
          }
          case('p'):
          {
            lux_threshold_lower = 4*command[2];
            lux_threshold_higher = 4*command[3];
            state=Waiting;
            break;
          }
        }
      }
      break;
    }
    case(Fallback):
    {
      current_lux=get_lux_Fallback();
      if(current_lux<lux_threshold_lower)
      {
        set_brightness(bright);  
      }
        if(current_lux>lux_threshold_higher)
      {
        set_brightness(off);  
      }
      state = Waiting;
      break;
    }
    case(Waiting):
    {
      break;
    }
  }
  if(act == 1)
    state = Active;
}
